# FEMA Funeral Assistance
This is a prototype interview to show certain concepts:
* a list of documents to deliver at the end
* showing OPs and adding them to a walkaway document
  * Separating OPs into individual MD files
* separating SOs into individual YAML files
* using `include` to pull the parts together