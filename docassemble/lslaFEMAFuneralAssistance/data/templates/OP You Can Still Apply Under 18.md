**You Can Still Apply If You Are Under 18**

If you are under 18, you can still get the FEMA COVID-19 Funeral Assistance. You must have personally paid for the funeral. Only the person that personally paid for the funeral can get this money.