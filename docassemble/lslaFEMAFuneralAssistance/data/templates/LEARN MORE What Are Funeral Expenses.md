Funeral expenses include the cost of a casket, clergy services, the use of the funeral home, headstone and burial plot. It also covers cremation and the cost of an urn.

Other funeral expenses are:
* Transportation 
* Transfer of remains
* Arrangement of the funeral ceremony
* Costs associated with gettingand certifying multiple death certificates
* Other expenses that the state or local government requires