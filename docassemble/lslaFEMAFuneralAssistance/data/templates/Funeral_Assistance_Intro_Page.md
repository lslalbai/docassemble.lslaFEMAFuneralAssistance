# Welcome To Lone Star Legal Aid’s Guide For FEMA Help With COVID-19 Funerals.

A new FEMA program is offering help paying for COVID-19 related funerals. Take this interview to find more information that could help you. **You can also apply for our services and see if you qualify for free legal services.**

This interview should take about 5 minutes. It will give you information about your situation as you answer the questions. If you want to apply with FEMA immediately, call **844-684-6333** | TTY: **800-462-7585**.