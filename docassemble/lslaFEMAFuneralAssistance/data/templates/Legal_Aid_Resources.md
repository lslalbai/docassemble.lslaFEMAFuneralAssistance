# How To Apply For Legal Aid

## Would You Like Help With Representing Yourself In Court?

Free legal services through legal aid are to help low-income people and families. You will not pay a legal aid lawyer for help. Often those who qualify for legal aid also qualify for a waiver of court costs. To see if you qualify for services through legal aid, please contact an agency like this one:

% if legal_aid == "LSLA" :
Lone Star Legal Aid (LSLA) is a law firm dedicated to helping low-income Texans with legal issues. Our legal services are intended to help low-income people and families. If you would like to see if you qualify for legal aid, please contact us at (800) 733-8394 or at [our online application](https://lslaoi.legalserver.org/modules/matter/extern_intake.php?pid=132&h=f0baaf&utm_campaign=SRL&utm_source=A2J&utm_medium=web). 

% elif legal_aid == "LANWT" :
Legal Aid of NorthWest Texas (LANWT) is a nonprofit organization that provides free civil legal help to low-income residents in 114 Texas counties throughout North and West Texas, with offices in Abilene, Amarillo, Brownwood, Dallas, Denton, Fort Worth, Lubbock, McKinney, Midland, Odessa, Plainview, San Angelo, Waxahachie, Weatherford, and Wichita Falls. If you would like to see if you qualify for legal aid, please contact LANWT at (888) 529-5277 or visit [the LANWT website](https://www.lanwt.org/).

% elif legal_aid == "TRLA" :
Texas RioGrande Legal Aid (TRLA) is the third largest legal aid provider in the United States and the largest in Texas. TRLA provides free civil legal services to residents in 68 Southwest Texas counties, and represents migrant and seasonal farm workers throughout the state and in six other southern states. If you would like to see if you qualify for legal aid, please contact TRLA at (888) 988-9996 or visit [the TRLA website](https://www.trla.org/).
% endif

Submitting an application does not mean the agency can find a lawyer to help you. The information in your application would be used to see if you qualify for free services, and even if you do qualify, the agency may not be able to assign you an attorney to help. If an attorney is available, you will be sent a representation agreement that you would need to sign and return before representation can start.

If you do not qualify for free service or if the agency above can’t find you a lawyer, you can contact the State Bar of Texas for a referral to a paid attorney at (800) 252-9690 (Toll Free), Monday - Friday, CST, 8:30 a.m. to 4:30 p.m. or at [the referral website](https://www.texasbar.com/lris/).