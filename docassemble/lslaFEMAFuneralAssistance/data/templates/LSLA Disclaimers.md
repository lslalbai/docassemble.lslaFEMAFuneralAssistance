# How Can This Site Help Me With My Legal Problem?
This site will guide you through a series of questions about your legal problem. Along the way, you’ll get free information and a summary of the interview at the end.

* Using this site will give you legal information, not legal advice. For legal advice, you need to consult an attorney.
* We will give you information that can connect you with free legal services if you qualify. 
* This site is free to all users. 

# What Can I Expect?
* We will use any information you provide to guide you to helpful legal information. 
* We will keep your information private as required by law. This includes any protected health information you might give us during your visit.
* You will get free legal information and resources to help you understand your legal problem.
* Using this site does not create an attorney-client relationship. 
* After the interview, you’ll be given contact information to see if you qualify for free legal services.
* If you apply for legal help, we cannot guarantee that you will get an attorney. 

